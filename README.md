# 我的基础AndroidX项目框架

#### 介绍
基于Androidx搭建的android基础开发框架
封装数据库操作、图片操作、网络请求操作
以及一些实用的第三方工具类


#组件化开发

#组件库使用的是CC


#buildSrc :三方依赖统一管理，第三方的依赖都放这个里面

## 基础库

#lib-switch : 网络切换库
#jdsdk_lib :京东联盟sdk  官方建议以lib的形式导入
#lib-core ：项目中最顶级的父类封装
#common-library：基础lib，以来lib-core,其他模块再依赖开发
#ocr_ui : 百度智能云ocr图片文字识别



##业务功能


1、新功能新创建一个Module ,创建 gradle.properties，
属性介绍:isRunAlone true 可以单独变异运行 [true 则需要在src/main/下创建debug 文件夹，以及对应的AndroidManifest.xml文件]
         debugComponent : 测试环境变异所需依赖模块[为独立开发的模块]
         compileComponent: 正式环境下所需要的的依赖模块

2、创建的新业务，最好以独立模块开发,后续可以以插件形式进行接入替代
   【新的路由路径定义在 com.artcollect.common.ArouterCommonContants】

3、mc.gradle 为动态压缩项目中大图片文件，若需要打开，直接取消注释 主module build.gradle //apply from : '../mc.gradle'

3、maven 为local仓库，勿删

4、根目录下gradle.properties  mainmodulename=app  勿删

5、统一依赖管理 buildSrc -> Dependencies.kt;  [build.gradle.kts 勿要修改、删除]


项目中 yw_1222_baichuan  勿删勿删勿删,阿里百川sdk集成必备




 ***************************************************[app主module]*********************************************************
                                                        |
                                                        |
 **[新Module模块]**
                                                        |
                                                        |
                                *******************【common-library】***********************
                                                         |
                                *******************【lib-core】***********************