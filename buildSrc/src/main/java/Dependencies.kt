object Versions {
    const val minSdkVersion = 21
    const val targetSdkVersion = 28
    const val compileSdkVersion = 28
    const val buildToolsVersion = "28.0.3"

    const val supportVersion = "28.0.0"

    const val version_code = 1
    const val version_name = "1.0"


    internal const val kotlin = "1.3.71"
    internal const val android_gradle_plugin = "3.5.0"

    internal const val retrofit = "2.4.0"
    internal const val rxjava = "2.2.6"
    internal const val rxandroid = "2.1.1"
    internal const val okhttp = "3.12.0"

    internal const val constraint_layout = "1.1.3"
    internal const val jetpack_version = "1.1.1"

}



object Deps {
    //kotlin
    const val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val android_gradle_plugin = "com.android.tools.build:gradle:${Versions.android_gradle_plugin}"



    //网络
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofit_rxjava = "com.squareup.retrofit2:adapter-rxjava:${Versions.retrofit}"
    const val retrofit_rxjava2 = "com.jakewharton.retrofit:retrofit2-rxjava2-adapter:1.0.0"
    const val retrofit_gson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val rxjava2 = "io.reactivex.rxjava2:rxjava:${Versions.rxjava}"
    const val rxandroid2 = "io.reactivex.rxjava2:rxandroid:${Versions.rxandroid}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
    const val okhttp_log = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
//    const val okhttp_log = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
//    const val rxjava1 = "io.reactivex:rxjava:1.1.6"
//    const val rxandroid1 = "io.reactivex:rxandroid:1.2.1"



    //jetPack
    const val lifetime_runtime = "android.arch.lifecycle:runtime:${Versions.jetpack_version}"
    const val lifecycle_compiler = "android.arch.lifecycle:compiler:${Versions.jetpack_version}"
    const val lifecycle_common_java8 = "android.arch.lifecycle:common-java8:${Versions.jetpack_version}"
    const val lifecycle_xtensions = "android.arch.lifecycle:extensions:${Versions.jetpack_version}"
    const val core_testing = "android.arch.core:core-testing:${Versions.jetpack_version}"
    const val lifecycle_reactivestreams = "android.arch.lifecycle:reactivestreams:${Versions.jetpack_version}"

    //ui
    const val app_compat_androidx="androidx.appcompat:appcompat:1.2.0";
    const val constraintlayout_androidx="androidx.constraintlayout:constraintlayout:2.0.2"
    const val recyclerview_androidx= "androidx.recyclerview:recyclerview:1.1.0"
    const val cardview="androidx.cardview:cardview:1.0.0";
    const val material="com.google.android.material:material:1.0.0"
    const val ChipsLayoutManager = "com.beloo.widget:ChipsLayoutManager:0.3.7@aar"
    //倒计时样式
    const val countdownView="com.github.iwgang:countdownview:2.1.6"
    //fragment
    const val fragmentation = "me.yokeyword:fragmentationx:1.0.2"

    const val zxing="cn.bingoogolapple:bga-qrcode-zxing:1.3.7"

    //其他
    //gilde
    const val glide_transformations = "jp.wasabeef:glide-transformations:4.1.0"
    const val glide = "com.github.bumptech.glide:glide:4.9.0"
    const val glide_complier = "com.github.bumptech.glide:compiler:4.9.0"
    const val glide_okhttp3 = "com.github.bumptech.glide:okhttp3-integration:4.9.0"
    const val glide_annotations = "com.github.bumptech.glide:annotations:4.9.0"
    const val PictureSelector="com.github.LuckSiege.PictureSelector:picture_library:v2.4.9"
    //    //gif
//    const val android_gif_drawable = "pl.droidsonroids.gif:android-gif-drawable:1.2.19"
    //圆角
//    const val corner_view = "com.makeramen:roundedimageview:2.3.0"
    //长图处理
    const val subsampling = "com.davemorrissey.labs:subsampling-scale-image-view-androidx:3.10.0"

    //immersionBar
    const val immersionbar = "com.gyf.immersionbar:immersionbar:3.0.0"
    //baseAdapter
    const val baseadapter = "com.github.CymChad:BaseRecyclerViewAdapterHelper:2.9.49"
    //multidex
    const val multidex = "androidx.multidex:multidex:2.0.0"


    //eventBus
    const val eventbus = "org.greenrobot:eventbus:3.2.0"
    //    //gson
    const val gson = "com.google.code.gson:gson:2.8.5"
//    const val fastjson="com.alibaba:fastjson:1.2.73"


    //greendao数据库
    const val greendao_gradle_plugin = "org.greenrobot:greendao-gradle-plugin:3.2.2"
    const val greendao = "org.greenrobot:greendao:3.2.2"

    //photoView
    const val phote_view = "com.github.chrisbanes:PhotoView:2.3.0"

    // 时间 地址 选择器
    const val PickerView = "com.contrarywind:Android-PickerView:4.1.9"
    //刷新 (核心必须依赖)
    const val SmartRefreshLayout_layout_kernel = "com.scwang.smart:refresh-layout-kernel:2.0.1"
    //刷新 (经典刷新头)
    const val SmartRefreshLayout_header_classics = "com.scwang.smart:refresh-header-classics:2.0.1"

    //banner
    const val banner = "com.youth.banner:banner:2.1.0"


    //    //屏幕适配
    const val autosize = "me.jessyan:autosize:1.2.1"
    //utilcode
    const val utilcode = "com.blankj:utilcodex:1.29.0"

    const val commons_collections4="org.apache.commons:commons-collections4:4.1"
    //谷歌权限库
    const val permission="pub.devrel:easypermissions:3.0.0"
    //图标库
    const val FlycoTabLayout_Lib = "com.flyco.tablayout:FlycoTabLayout_Lib:2.1.2@aar"


//    // 仅在debug包启用BlockCanary进行卡顿监控和提示的话
//    const val debugblockcanary = "com.github.markzhai:blockcanary-android:1.5.0"
//    const val releaseblockcanary = "com.github.markzhai:blockcanary-no-op:1.5.0"
//
//    const val debugleakcanary = "com.squareup.leakcanary:leakcanary-android:1.5.4"
//    const val releaseleakcanary = "com.squareup.leakcanary:leakcanary-android-no-op:1.5.4"



    //arouter
    const val arouter = "com.alibaba:arouter-api:1.5.0"
    const val arouter_compiler = "com.alibaba:arouter-compiler:1.2.2"


//    //浮动
//    const val MagicIndicator = "com.github.hackware1993:MagicIndicator:1.5.0"

    //崩溃分析——腾讯Bugly
//    const val bugly_crashreport = "com.tencent.bugly:crashreport:3.3.3"
//    const val bugly_native_crashreport = "com.tencent.bugly:nativecrashreport:3.7.500"

    //butterKif
    const val butterknifeCompiler = "com.jakewharton:butterknife-compiler:10.2.3"
    const val butterknife = "com.jakewharton:butterknife:10.2.3"

    //推送——个推
    const val getui="com.getui:sdk:2.14.2.0"
}