文件目录说明
activity：所有页面
————main：首页

adapter：所有适配器
base：所有基础类
dialog：对话框
finals：所有常量
intf：所有接口
model：所有数据模型
net：网络请求框架
util：常用工具
view：自定义视图


第一次clone git 项目后需要解决仓库冲突
git pull <远程主机名> <本地分支名> --allow-unrelated-histories
git pull origin master --allow-unrelated-histories

以上是将远程仓库的文件拉取到本地仓库了。
紧接着将本地仓库的提交推送到远程github仓库上，使用的命令是：

$ git push <远程主机名> <本地分支名>:<远程分支名>
也就是
$git push origin master:master
提交成功。


获取sha1
 keytool -list -v -keystore debug.keystore

 项目Module说明
 app                        主项目目录
 buildSrc                   基础项目资源module
 --Dependencies.kt          附属依赖kotlin管理
    --Versions              项目版本管理
    --Deps                  项目依赖版本管理
 common-library             公共第三方插件工具管理
 --com.artcollect.common    公共工具管理
    --adapter               公共工具适配器管理
    --arouter               跳转路由管理
    --cache                 数据缓存管理(用户信息、设置信息)
    --config                应用常量管理(请求地址)
    --dialog                常用弹框管理
    --http                  网络请求
    --interf                接口管理
    --module                数据管理
    --refresh               刷新管理
    --utils                 工具类管理
    --widgets               自定义视图管理
