package com.qgqaqgqa.mybaseprojectx.util;

import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.TimeUtils;

/**
 * User: Created by 钱昱凯
 * Date: 2018/4/14/0014
 * Time: 11:50
 * EMail: 342744291@qq.com
 */

public class MyTimeUtils {
    public static String millis2String(String timeMillis) {
        if (StringUtils.isEmpty(timeMillis) || "null".equals(timeMillis) ||
                "0".equals(timeMillis)) {
            return "";
        } else if (timeMillis.contains(" ") || timeMillis.contains("-") ||
                timeMillis.contains(":")) {
            return timeMillis;
        } else if (timeMillis.length() == 10) {
            return TimeUtils.millis2String(Long.parseLong(timeMillis) * 1000);
        } else {
            return TimeUtils.millis2String(Long.parseLong(timeMillis));
        }
    }

    public static String millis2String(int timeMillis) {
        return millis2String(timeMillis + "");
    }

    public static String millis2String(long timeMillis) {
        return millis2String(timeMillis + "");
    }
}
