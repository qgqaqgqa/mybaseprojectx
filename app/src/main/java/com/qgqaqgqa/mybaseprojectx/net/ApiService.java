package com.qgqaqgqa.mybaseprojectx.net;
import java.util.List;
import java.util.Map;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * 所有接口的入口
 * User: Created by 钱昱凯
 * Date: 2018/2/4/0004
 * Time: 18:40
 * EMail: 342744291@qq.com
 */
public interface ApiService {
    ///////////////////////////////////////////////////////////////////////////
    // Retrofit注解详解
    // HTTP请求方法：
    //      GET、POST、PUT、DELETE、PATCH、HEAD、OPTIONS、HTTP
    // 标记类：
    //      FormUrlEncoded表示请求体是From表单
    //      Multipart表示请求体是支持上传的表单
    //      streaming表示响应体是流的形式如果数据太大用这种方式
    // 参数类：
    //      Headers用于请求头
    //      Header用于添加不固定值的Header
    //      body用于非表单请求体
    //      Field、FieldMap用于表单字段与FormUrlEncoded配合
    //      FieldMap接受类型Map<String, String>
    //      Part、PartMap用于文件上传与Multipart配合
    //      PartMap接受类型Map<String, RequestBody>
    //      Path用于URL
    //      Query、QueryMap与Field、FieldMap功能一样不同在于数据体现在URL上
    //      Url
    //      body用于非表单请求体
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Retrofit的Url组合规则
    // BaseUrl 和URL有关的注解中提供的值
    // 最后结果
    // http://localhost:4567/path/to/other/	/post	http://localhost:4567/post
    // http://localhost:4567/path/to/other/	post	http://localhost:4567/path/to/other/post
    // http://localhost:4567/path/to/other/	https://github.com/ikidou	https://github.com/ikidou
    // 从上面不能难看出以下规则：
    //
    // 如果你在注解中提供的url是完整的url，则url将作为请求的url。
    // 如果你在注解中提供的url是不完整的url，且不以 /开头，则请求的url为baseUrl+注解中提供的值
    // 如果你在注解中提供的url是不完整的url，且以 /开头，则请求的url为baseUrl的主机部分+注解中提供的值
    ///////////////////////////////////////////////////////////////////////////

    @GET()
    Observable<ResponseBody> get(@Url String url, @HeaderMap Map<String, String> headers, @QueryMap Map<String, Object> maps);

    @GET()
    Observable<ResponseBody> get(@Url String url, @HeaderMap Map<String, String> headers, @Body RequestBody requestBody);

    /**
     * get请求发送json数据
     * @param url
     * @param requestBody
     * @return
     */
    @GET()
    Observable<ResponseBody> get2Json(@Url String url, @HeaderMap Map<String, String> headers, @Body RequestBody requestBody);

    /**
     * 注意:
     * 1.如果方法的泛型指定的类不是ResonseBody,retrofit会将返回的string成用json转换器自动转换该类的一个对象,转换不成功就报错.
     *  如果不需要gson转换,那么就指定泛型为ResponseBody,
     *  只能是ResponseBody,子类都不行,同理,下载上传时,也必须指定泛型为ResponseBody
     * 2. map不能为null,否则该请求不会执行,但可以size为空.
     * 3.使用@url,而不是@Path注解,后者放到方法体上,会强制先urlencode,然后与baseurl拼接,请求无法成功.
     * @param url
     * @param map
     * @return
     */
    @FormUrlEncoded
    @POST()
    Observable<ResponseBody> post(@Url String url, @HeaderMap Map<String, String> headers, @FieldMap Map<String, Object> map);
    /**
     * post请求发送json数据
     * @param url
     * @param requestBody
     * @return
     */
    @POST()
    Observable<ResponseBody> post(@Url String url, @HeaderMap Map<String, String> headers, @Body RequestBody requestBody);

    /**
     * post请求发送json数据
     * @param url
     * @param requestBody
     * @return
     */
    @POST()
    Observable<ResponseBody> post2Json(@Url String url, @HeaderMap Map<String, String> headers, @Body RequestBody requestBody);

    @POST()
    @Multipart
    Observable<ResponseBody> post(@Url String url, @HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> map,
                                  @Part MultipartBody.Part file);

    @POST()
    @Multipart
    Observable<ResponseBody> post(@Url String url, @HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> map,
                                  @Part List<MultipartBody.Part> fileList);

    @DELETE()
    Observable<ResponseBody> delete(@Url String url, @HeaderMap Map<String, String> headers, @Query("id") String id);

    @PUT()
    Observable<ResponseBody> put(@Url String url, @HeaderMap Map<String, String> headers, @QueryMap Map<String, Object> maps);

    @PUT()
    Observable<ResponseBody> put(@Url String url, @HeaderMap Map<String, String> headers, @Body RequestBody requestBody);
}