package com.qgqaqgqa.mybaseprojectx.net;

import org.reactivestreams.Subscription;

/**
 * 基础请求返回接口
 * User: Created by 钱昱凯
 * Date: 2018/2/9 0009
 * Time: 14:19
 * Email:342744291@qq.com
 */

public interface IBaseHttpCallBack {

    /**
     * 成功时回调
     * @param url 请求url
     * @param response 请求数据模型
     */
    void onSucess(String url, BaseNetModel response);

    /**
     * 回调完成时结束请求等待框
     */
    void onFinish();
}
