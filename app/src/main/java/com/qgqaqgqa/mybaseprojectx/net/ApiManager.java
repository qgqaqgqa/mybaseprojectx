package com.qgqaqgqa.mybaseprojectx.net;

import android.graphics.Bitmap;
import android.os.Build;
import android.util.Base64;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.qgqaqgqa.mybaseprojectx.finals.AppConfig;

///////////////////////////////////////////////////////////////////////////
// Retrofit的Url组合规则
//
//    BaseUrl 和URL有关的注解中提供的值
//    最后结果
//    http://localhost:4567/path/to/other/	/post	http://localhost:4567/post
//    http://localhost:4567/path/to/other/	post	http://localhost:4567/path/to/other/post
//    http://localhost:4567/path/to/other/	https://github.com/ikidou	https://github.com/ikidou
//    从上面不能难看出以下规则：
//
//    如果你在注解中提供的url是完整的url，则url将作为请求的url。
//    如果你在注解中提供的url是不完整的url，且不以 /开头，则请求的url为baseUrl+注解中提供的值
//    如果你在注解中提供的url是不完整的url，且以 /开头，则请求的url为baseUrl的主机部分+注解中提供的值
//
///////////////////////////////////////////////////////////////////////////

/**
 * 接口管理
 * User: Created by 钱昱凯
 * Date: 2018/2/4/0004
 * Time: 18:46
 * EMail: 342744291@qq.com
 */
public class ApiManager {
    private Gson mGson;
    private ApiService service;
    private static ApiManager apiManager;

    private ApiManager() {
        mGson = new Gson();
    }

    /**
     * 单利模式
     *
     * @return
     */
    public synchronized static ApiManager getInstance() {
        if (apiManager == null) {
            apiManager = new ApiManager();
        }
        return apiManager;
    }

    /**
     * 创建一个apiservice
     *
     * @return
     */
    public ApiService createApiService() {
        if (service == null) {
            OkHttpClient.Builder okHttpClient = null;
            okHttpClient = new OkHttpClient.Builder();
            if (AppConfig.TEST_MODE) {
                // log用拦截器
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                // 开发模式记录整个body，否则只记录基本信息如返回200，http协议版本等
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                okHttpClient.addInterceptor(logging);//添加log拦截器
                okHttpClient.addNetworkInterceptor(new CacheInterceptor());//添加网络缓存
            } else {
            }

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConfig.ZAPI_URL)
                    .client(okHttpClient.build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = retrofit.create(ApiService.class);
        }
        return service;
    }

    //region==============请求方式=====================
    public Disposable get(Class<?> tClass, String url, IBaseHttpCallBack callBack) {
        return get(tClass, url, null, callBack);
    }

    public Disposable get(Class<?> tClass, String url, Map<String, Object> maps, IBaseHttpCallBack callBack) {
        return get(tClass, null, url, maps, callBack);
    }

    public Disposable getList(Class<?> tClass, String url, IBaseHttpCallBack callBack) {
        return getList(tClass, url, null, callBack);
    }

    public Disposable getList(Class<?> tClass, String url, Map<String, Object> maps, IBaseHttpCallBack callBack) {
        return get(null, tClass, url, maps, callBack);
    }

    /**
     * 发送get请求
     * @param cls 请求结果data是对象
     * @param element 请求结果data是列表数组
     * @param url 请求路径
     * @param maps 请求参数
     * @param callBack 请求成功和结束回调
     * @return 返回Disposable对象，在activity结束时执行{@link Disposable#dispose()}取消请求继续发送
     */
    private Disposable get(Class<?> cls, Class<?> element, String url, Map<String, Object> maps, IBaseHttpCallBack callBack) {
        return initFlowable(createApiService().get(url,getHeaders(), initMap(url, maps)), cls, element, url, callBack);
    }

    public Disposable post(Class<?> tClass, String url, IBaseHttpCallBack callBack) {
        return post(tClass, url, null, null, callBack);
    }
    public Disposable post(Class<?> tClass, String url, Object o, IBaseHttpCallBack callBack) {
        if(o instanceof String)
            return post(tClass, url, null, (String) o, callBack);
        else
            return post(tClass, url, o, null, callBack);
    }

    public Disposable post(Class<?> tClass, String url, Object o, String file, IBaseHttpCallBack callBack) {
        return post(tClass,null, url, o, file, callBack);
    }

    public Disposable postList(Class<?> tClass, String url, IBaseHttpCallBack callBack) {
        return postList(tClass, url, null, null, callBack);
    }

    public Disposable postList(Class<?> tClass, String url, Object o, IBaseHttpCallBack callBack) {
        return o instanceof String?
                postList(tClass, url, null, (String) o, callBack):
                postList(tClass, url, o, null, callBack);
    }

    public Disposable postList(Class<?> tClass, String url, Object o, String file, IBaseHttpCallBack callBack) {
        return post(null, tClass, url, o, file, callBack);
    }

    /**
     * 发送post请求
     * @param cls 请求结果data是对象
     * @param element 请求结果data是列表数组
     * @param url 请求路径
     * @param o 请求参数 如果请求参数类型属于Map就试用key、value键值对形式传递，
     *          否则修改contentType为application/json，将数据对象整个传递过去
     * @param callBack 请求成功和结束回调
     * @return 返回Disposable对象，在activity结束时执行{@link Disposable#dispose()}取消请求继续发送
     */
    private Disposable post(Class<?> cls, Class<?> element, String url,
                            Object o, String file, final IBaseHttpCallBack callBack) {
        if(o instanceof Map){
            Map<String, Object> maps = initMap(url, (Map<String, Object>) o);
            if (file != null) {
                Map<String, RequestBody> fileUpload2Args = new HashMap<>();
                for (String key : maps.keySet()) {
                    fileUpload2Args.put(key, convertToRequestBody((String) maps.get(key)));
                }
                return initFlowable(createApiService().post(url,getHeaders(), fileUpload2Args,
                        filesToMultipartBodyParts("fileBase64", new File(file))), cls, element, url, callBack);
            } else{
                return initFlowable(createApiService().post(url,getHeaders(), maps), cls, element, url, callBack);
            }
        }else{
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),mGson.toJson(o));
            return initFlowable(createApiService().post2Json(url,getHeaders(), requestBody), cls, element, url, callBack);
        }
    }
    public Disposable delete(Class<?> cls, Class<?> element, String url, String id, IBaseHttpCallBack callBack) {
        return initFlowable(createApiService().delete(url,getHeaders(), id), cls, element, url, callBack);
    }

    public Disposable put(String url, Object o, IBaseHttpCallBack callBack) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),mGson.toJson(o));
        return initFlowable(createApiService().put(url,getHeaders(), requestBody), null, null, url, callBack);
    }

    //endregion

    /**
     *只传传obj参数的统一请求
     */
    public static Map<String, String> getHeaders() {
        Map<String, String> map = new HashMap<>();
//        map.put("Content-Type", "application/json");
//        map.put("Accept", "application/json");
//        map.put("token", UserInfoCache.getToken());
//        map.put("deviceId", DeviceHelper.getDeviceId(AppContext.getContext()));
//        map.put("language",UserInfoCache.getlanguage());
//        map.put("lon", "");
//        map.put("lat","");
        return map;
    }

    private Map<String, Object> initMap(String url, Map<String, Object> maps) {
        if (maps == null) {
            maps = new HashMap<>();
        }
//        maps.put(UcenterLoginMembersSerializable.MEMBER_TOKEN,
//                SPUtils.getInstance().getString(UcenterLoginMembersSerializable.MEMBER_TOKEN));
//        if(NetworkUtils.isConnected()){
//            NetworkUtils.getIPAddress(true);
//            DeviceUtils.getMacAddress();
//            DeviceUtils.getSDKVersionName();
//            DeviceUtils.getSDKVersionCode();
//            AppUtils.getAppPackageName();
//            AppUtils.getAppName();
//            AppUtils.getAppVersionName();

//        }
        String deviceStartTime = SPUtils.getInstance().getString("deviceStartTime");
        if (StringUtils.isEmpty(deviceStartTime)) {
            deviceStartTime = System.currentTimeMillis() + "";
            SPUtils.getInstance().put("deviceStartTime", deviceStartTime);
        }
        maps.put("clientDeviceNum", EncryptUtils.encryptMD5ToString(
                DeviceUtils.getAndroidID() + Build.SERIAL + deviceStartTime));
        maps.put("andrdoidId", DeviceUtils.getAndroidID());
        maps.put("clientOs", "android");
        maps.put("clientType", "android");
        maps.put("clientAppVerison", AppUtils.getAppVersionName());
        maps.put("clientIp", NetworkUtils.getIPAddress(true));
        LogUtils.e(url, maps);
        return maps;
    }

    private Disposable initFlowable(Observable<ResponseBody> observable, final Class<?> cls, final Class<?> element, final String url, final IBaseHttpCallBack callBack) {
        return observable.subscribeOn(Schedulers.newThread())
                .map(new Function<ResponseBody, BaseNetModel>() {
                    @Override
                    public BaseNetModel apply(ResponseBody body) throws Exception {
                        BaseNetModel bm = null;
                        try {
                            String data = body.string();
                            bm = parseData(data, cls, element);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return bm;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<BaseNetModel>() {
                    @Override
                    public void accept(@NonNull BaseNetModel bm) throws Exception {
                        if(bm!=null){
                            if (bm.getErrcode() == 0) {//获取数据成功
                                if (callBack != null) {
                                    if (element != null) {
                                        if (bm.getData() == null) {
                                            bm.setData(new ArrayList<>());
                                        }
                                    }
                                    callBack.onSucess(url, bm);
                                }
                            } else if (bm.getErrcode() == 44444) {//账号异常请重新登录
                                //token失效
                            } else if (bm.getErrcode() > 0) {//弹出消息
                                ToastUtils.showShort(bm.getMsg());
                            } else {
                            }
                        }
                        if (callBack != null) {
                            callBack.onFinish();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable o) {
                        if (callBack != null) {
                            callBack.onFinish();
                        }
                        ToastUtils.showLong("网络或服务异常");
                    }
                });
    }

    /**
     * 数据解析，如果需要解析，则重写这个方法
     *
     * @return
     */
    public BaseNetModel parseData(String data, Class<?> cls, Class<?> element) throws JsonSyntaxException {
        LogUtils.e(data);
        //fromJson()方法用于将JSON数据转换为相应的Java对象
        //或者把json格式的data转化成BaseModel类型
        //基本信息（即basemodel中信息，只附带成功和失败）
        BaseNetModel base = mGson.fromJson(data, BaseNetModel.class);
        if (base.isSuccess()) {
            Type t = null;
            if (cls != null) {
                t = com.google.gson.internal
                        .$Gson$Types
                        .newParameterizedTypeWithOwner(null, BaseNetModel.class, cls);
                return mGson.fromJson(data, t);
            } else {
                if (element != null) {
                    t = com.google.gson.internal
                            .$Gson$Types
                            .newParameterizedTypeWithOwner(
                                    null,
                                    BaseNetModel.class,
                                    com.google.gson.internal.$Gson$Types.newParameterizedTypeWithOwner(null, ArrayList.class, element)
                            );
                    return mGson.fromJson(data, t);
                }
            }
        }
        return base;
    }

    public static RequestBody convertToRequestBody(String param) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), param);
        return requestBody;
    }

    public static MultipartBody.Part filesToMultipartBodyParts(File file) {
        return filesToMultipartBodyParts("multipartFiles", file);
    }

    public static MultipartBody.Part filesToMultipartBodyParts(String name, File file) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/otcet-stream"), file);
        return MultipartBody.Part.createFormData(name, file.getName(), requestBody);
    }

    public static List<MultipartBody.Part> filesToMultipartBodyParts(List<File> files) {
        List<MultipartBody.Part> parts = new ArrayList<>(files.size());
        for (File file : files) {
            parts.add(filesToMultipartBodyParts(file));
        }
        return parts;
    }

    /**
     * 通过Base32将Bitmap转换成Base64字符串
     *
     * @param bit
     * @return
     */
    public static String Bitmap2StrByBase64(Bitmap bit) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bit.compress(Bitmap.CompressFormat.PNG, 40, bos);//第二个入参表示图片压缩率，如果是100就表示不压缩
        byte[] bytes = bos.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

}