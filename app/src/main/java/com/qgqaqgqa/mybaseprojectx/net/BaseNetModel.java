package com.qgqaqgqa.mybaseprojectx.net;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * User: Created by 钱昱凯
 * Date: 2018/2/4/0004
 * Time: 19:16
 * EMail: 342744291@qq.com
 */
public class BaseNetModel<T> implements Serializable {
    //接口错误代码
    private int code;
    //接口错误代码
    private int errcode;

    //接口错误信息
    private String msg;

    //接口返回的数据模型，我们实际需要的数据值
    private T data;

    //请求代码
    private String requestcode;

    //列表总数
    private String totalCount;

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getRequestcode() {
        return requestcode;
    }

    public void setRequestcode(String requestcode) {
        this.requestcode = requestcode;
    }

    /**
     * 创建一个Class的对象来获取泛型的class
     */
    private Class<T> clz;

    @SuppressWarnings("unchecked")
    public Class<T> getClz() {
        if (clz == null) {
            clz = (Class<T>) (((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
        }
        return clz;
    }

    public boolean isSuccess() {
        return 0 == errcode || 200 == code;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }
}
