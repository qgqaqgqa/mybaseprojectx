package com.qgqaqgqa.mybaseprojectx.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * 基础数据处理
 * Created by qian on 2020/10/19 0001.
 */
public class BaseDao<T extends BaseBean> implements Serializable {
    private static Object OSSLock = new Object();
    private DBOpenHelper helper;
    protected int defaultPageSize=30;
    protected String beanTitle = "base";
    private String sqlWhere;

    public void setDefaultPageSize(int defaultPageSize) {
        this.defaultPageSize = defaultPageSize;
    }

    /**
     *
     * @param paramContext
     * @param title @See{DemoBean.TITLE}
     * @param clz @See{DemoBean.class}
     */
    public BaseDao(Context paramContext, String title,Class<T> clz) {
        this.helper = new DBOpenHelper(paramContext);
        beanTitle=title;
        this.clz=clz;
    }

    private String getTitle(){
        return beanTitle;
    }

    protected T newBeanInstance(Cursor cursor){
        try {
            return clz.newInstance().initBaseDate(cursor);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 创建一个Class的对象来获取泛型的class
     */
    private Class<T> clz;

    /**
     * 删除所有
     *
     * @return
     */
    public int deleteAll() {
        return deleteAll(null,null);
    }
    public int deleteAll(String where, String[] values) {
        synchronized (OSSLock) {
            SQLiteDatabase db = this.helper.getWritableDatabase();
            int i = db.delete(getTitle(), where, values);
            db.close();
            return i;
        }
    }

    /**
     * 删除所有
     *
     * @return
     */
    public int deleteOld(String time) {
        synchronized (OSSLock) {
            SQLiteDatabase db = this.helper.getWritableDatabase();
            int i = db.delete(getTitle(), BaseBean.COLUMN_NAME_CREATE_DATE+" < ?", new String[]{time});
            db.close();
            return i;
        }
    }

    public long insert(BaseBean m) {
        delete(m.getId());
        synchronized (OSSLock) {
            SQLiteDatabase db = this.helper.getWritableDatabase();
            long l = db.insert(getTitle(), null, m.getBaseContentValues());
            db.close();
            return l;
        }
    }
    /**
     * 查询记录总数
     *
     * @return
     */public long getCount() {
        SQLiteDatabase db = this.helper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select count(id) from "+getTitle(),null);
        cursor.moveToFirst();
        Long count = cursor.getLong(0);
        cursor.close();
        return count;
    }
    /**
     * 根据Id查询数据
     *
     * @return
     */
    public T queryById(String id) {
        synchronized (OSSLock) {
            SQLiteDatabase db = this.helper.getReadableDatabase();
            ArrayList<T> areaList = new ArrayList<>();
            Cursor cursor = db.query(getTitle(), null,
                    BaseBean.COLUMN_NAME_ID+"=?", new String[]{id}, null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    areaList.add(newBeanInstance(cursor));
                }
            }
            cursor.close();
            db.close();
            if(areaList.size()>0)
                return areaList.get(0);
            return null;
        }
    }

    /**
     * 查询全部项目
     *
     * @return
     */
    public List<T> query() {
        return query(null);
    }
    public List<T> query(HashMap<String, String> map) {
        return query(map,-1);
    }
    public List<T> query(HashMap<String, String> map, int page) {
        return query(map,page,BaseBean.COLUMN_NAME_CREATE_DATE+" ASC");
    }
    public List<T> queryList(int page) {
        return query(null,page,BaseBean.COLUMN_NAME_CREATE_DATE+" ASC");//+" DESC");
    }
    public List<T> queryList(int page, String orderBy) {
        return query(null,page,orderBy);
    }

    /**
     * 根据map参数查询数据
     *
     * @return
     */
    public List<T> query(HashMap<String, String> map, int page, String orderBy) {
        synchronized (OSSLock) {
            SQLiteDatabase db = this.helper.getReadableDatabase();
            StringBuffer sb=new StringBuffer();
            String[] strArray = null;
            if(map!=null){
                Set<String> set = map.keySet();
                strArray=set.toArray(new String[set.size()]);
                for(String key:set){
                    if(sb.length()>0)
                        sb.append(" AND ");
                    sb.append(key).append("=?");
                }
                for(int i=0;i<set.size();i++){
                    strArray[i]=map.get(strArray[i]);
                }
            }
            Cursor cursor = db.query(getTitle(), null,
                    sb.length()==0?null:sb.toString(),
                    sb.length()==0?null:strArray, null, null, orderBy,getLimit(page));
            ArrayList<T> areaList = new ArrayList<>();
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    areaList.add(newBeanInstance(cursor));
                }
            }
            cursor.close();
            db.close();
            return areaList;
        }
    }

    private String getLimit(int page){
        return page==-1?null:page*defaultPageSize+","+defaultPageSize;
    }


    /**
     * 删除
     *
     * @return
     */
    public int delete(String id) {
        synchronized (OSSLock) {
            SQLiteDatabase db = this.helper.getWritableDatabase();
            int i = db.delete(getTitle(), BaseBean.COLUMN_NAME_ID+"=?",
                    new String[]{id});
            db.close();
            return i;
        }
    }
    /**
     * 删除
     *
     * @return
     */
    public int deleteWhere(String where) {
        synchronized (OSSLock) {
            SQLiteDatabase db = this.helper.getWritableDatabase();
            int i = db.delete(getTitle(), where,
                    null);
            db.close();
            return i;
        }
    }
}
