package com.qgqaqgqa.mybaseprojectx.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "class.db";
    public static final int DATABASE_VERSION = 1;

    /**
     * 例子数据
     */
    public static final String TABLE_DEMO = "demo";

    private String TAG = getClass().getSimpleName();

    public DBOpenHelper(Context paramContext) {
        super(paramContext, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void close(SQLiteDatabase paramSQLiteDatabase) {
        if (paramSQLiteDatabase != null) ;
        try {
            if (paramSQLiteDatabase.isOpen())
                paramSQLiteDatabase.close();
        } catch (Exception localException) {
            while (true)
                Log.e(this.TAG, "close fail--->>" + localException.getMessage());
        }
    }

    public void closeDatabase() {
        try {
            close();
        } catch (Exception localException) {
            while (true)
                Log.e(this.TAG, "closeDatabase close fail-->>" + localException.getMessage());
        }
    }

    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL("CREATE TABLE " + TABLE_DEMO + " (id INTEGER primary key autoincrement ," +
                    "create_date INTEGER,content VARCHAR,update_date INTEGER);");
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            while (true) {
                Log.e(this.TAG, ":insert fail" + e.getMessage());
                db.endTransaction();
            }
        } finally {
            db.endTransaction();
        }
    }

    public void onUpgrade(SQLiteDatabase db, int paramInt1, int paramInt2) {
        db.execSQL("drop table if exists "+ TABLE_DEMO);
        onCreate(db);
    }
}
