package com.qgqaqgqa.mybaseprojectx.db;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;

/**
 * 基础数据类
 */
public class BaseBean implements Serializable {
    public static final String TITLE = "base";
    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_CREATE_DATE = "create_date";
    protected String id;
    protected long createDate=-1;

    public String getId() {
        return id==null?"":id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate==-1? System.currentTimeMillis():createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    /**
     * 将对象转换成ContentValues插入数据库
     * @return
     */
    public ContentValues getContentValues(){
        return new ContentValues();
    }

    protected ContentValues getBaseContentValues() {
        ContentValues contentValues = getContentValues();
        contentValues.put(COLUMN_NAME_ID,id);
        contentValues.put(COLUMN_NAME_CREATE_DATE, getCreateDate());
        return contentValues;
    }

    public void initDate(Cursor cursor){

    }

    public <T extends BaseBean> T initBaseDate(Cursor cursor) {
        initDate(cursor);
        id=cursor.getString(cursor.getColumnIndex(COLUMN_NAME_ID));
        createDate= cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_CREATE_DATE));
        return (T) this;
    }
}
