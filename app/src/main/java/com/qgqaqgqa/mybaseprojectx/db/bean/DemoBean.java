package com.qgqaqgqa.mybaseprojectx.db.bean;

import android.content.ContentValues;
import android.database.Cursor;

import com.qgqaqgqa.mybaseprojectx.db.BaseBean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 课程名称信息
 * Created by qian on 2020/10/19.
 */
public class DemoBean extends BaseBean implements Serializable {
    public static final String TITLE = "demo";
    private static final long serialVersionUID = 1L;
    public static final String COLUMN_NAME_CONTENT = "content";
    public static final String COLUMN_NAME_SCHEDULE_ID = "schedule_id";


    /**
     * 课程名称
     */
    protected String content;
    /**
     * 课表id
     */
    protected String scheduleId;

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getContent() {
        return content==null?"":content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME_CONTENT,content);
        contentValues.put(COLUMN_NAME_SCHEDULE_ID,scheduleId);
        return contentValues;
    }

    @Override
    public void initDate(Cursor cursor) {
        content=cursor.getString(cursor.getColumnIndex(COLUMN_NAME_CONTENT));
        scheduleId=cursor.getString(cursor.getColumnIndex(COLUMN_NAME_SCHEDULE_ID));
    }
}
