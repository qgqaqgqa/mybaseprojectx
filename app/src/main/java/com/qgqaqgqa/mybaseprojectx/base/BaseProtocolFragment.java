package com.qgqaqgqa.mybaseprojectx.base;

import com.qgqaqgqa.mybaseprojectx.net.IBaseHttpCallBack;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * 基础请求Activity
 * User: Created by 钱昱凯
 * Date: 2018/1/18 0018
 * Time: 16:14
 */
public abstract class BaseProtocolFragment extends BaseFragment implements IBaseHttpCallBack {
    private CompositeDisposable composite = new CompositeDisposable();

    public void addDisposable(Disposable disposable) {
        composite.add(disposable);
    }

    public void clearDisposable() {
        composite.dispose();
    }
    @Override
    public void onFinish() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearDisposable();
    }
}