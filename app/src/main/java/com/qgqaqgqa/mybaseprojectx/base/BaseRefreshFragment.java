package com.qgqaqgqa.mybaseprojectx.base;

import com.qgqaqgqa.mybaseprojectx.R;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import butterknife.BindView;

/**
 * User: Created by 钱昱凯
 * Date: 2019/11/20
 * Time: 15:02
 * EMail: 342744291@qq.com
 */
public abstract class BaseRefreshFragment extends BaseProtocolFragment implements OnRefreshListener {

    @BindView(R.id.refreshLayout)
    protected SmartRefreshLayout mRefreshLayout;
//    @BindView(R.id.recyclerview)
//    RecyclerView recyclerview;


    @Override
    public int getLayoutResource() {
        return R.layout.custom_refresh_layout;
    }


    public void findIds() {
        mRefreshLayout.setEnableLoadMore(false);//关闭加载更多

        mRefreshLayout.setOnRefreshListener(this);
    }

    public void updateView() {
        refreshView();
    }

    public void refreshView() {
        if(mRefreshLayout!=null)
        mRefreshLayout.autoRefresh();
    }

//    private void refreshView() {
//    }

//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (hidden) {
//            // 不在最前端界面显示
//
//        } else {
//            // 重新显示到最前端中
//        }
//    }

    @Override
    public void onFinish() {
        super.onFinish();
        if (mRefreshLayout != null) {
            if (mRefreshLayout.isRefreshing()) {
                mRefreshLayout.finishRefresh();
            }

            if (mRefreshLayout.isLoading()) {
                mRefreshLayout.finishLoadMore();
            }
        }
    }
}
