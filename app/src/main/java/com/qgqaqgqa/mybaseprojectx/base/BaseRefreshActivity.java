package com.qgqaqgqa.mybaseprojectx.base;

import com.qgqaqgqa.mybaseprojectx.R;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import butterknife.BindView;

/**
 * User: Created by 钱昱凯
 * Date: 2019/11/20
 * Time: 15:02
 * EMail: 342744291@qq.com
 */
public abstract class BaseRefreshActivity extends BaseProtocolActivity implements OnRefreshListener {

    @BindView(R.id.refreshLayout)
    protected SmartRefreshLayout mRefreshLayout;

    public BaseRefreshActivity(int layoutResID) {
        super(layoutResID);
    }

    public void findIds() {
        mRefreshLayout.setEnableLoadMore(false);//关闭加载更多

        mRefreshLayout.setOnRefreshListener(this);
    }

    public void updateView() {
        refreshView();
    }

    public void refreshView() {
        mRefreshLayout.autoRefresh();
    }

    @Override
    public void onFinish() {
        super.onFinish();
        if (mRefreshLayout != null) {
            if (mRefreshLayout.isRefreshing()) {
                mRefreshLayout.finishRefresh();
            }

            if (mRefreshLayout.isLoading()) {
                mRefreshLayout.finishLoadMore();
            }
        }
    }
}
