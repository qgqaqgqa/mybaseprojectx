package com.qgqaqgqa.mybaseprojectx.activity;
import android.os.Build;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import java.util.HashMap;

import com.blankj.utilcode.util.AppUtils;
import com.qgqaqgqa.mybaseprojectx.R;
import com.qgqaqgqa.mybaseprojectx.base.BaseActivity;
import com.qgqaqgqa.mybaseprojectx.view.CommonTitleBar;
import com.qgqaqgqa.mybaseprojectx.view.FramentWebView;

/**
 * app内部显示网页，webView
 * User: Created by 钱昱凯
 * Date: 2018/3/8 0008
 * Time: 16:01
 * Email:342744291@qq.com
 */
public class MyWebActivity extends BaseActivity {
    public String TAG = this.getClass().getName();
    private FramentWebView webView;
    private String url;
    private String data;
    private String title;

    public MyWebActivity() {
        super(R.layout.act_my_web);
    }

    @Override
    public void getIntentData() {
        HashMap<String, String> map = (HashMap<String, String>) getIntent().getSerializableExtra("data");
        url = map.get("url");
        data = map.get("data");
        title = map.get("title");
    }

    @Override
    public void initViews() {
        mTitle = new CommonTitleBar(this);
        mTitle.setTitle(title == null ? "详情" : title);
        mTitle.showLeftIcon();
        webView = new FramentWebView(this, new FramentWebView.WebViewCallBack() {
            @Override
            public boolean urlCallBack(WebView web, String url) {
                //只有在调用webview.loadURL的时候才会调用
                //重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
                //return false 跳浏览器去

//                if(url.contains("?")){
//                    url+="?";
//                }else {
//                    url+="&";
//                }
//                if(!url.contains("clientType=app")){
//                    url+="clientType=app";
//                }
//                webView.loadUrl(url);
//                return true;
                return false;
            }
        });
//        addJavascriptInterface();

        if (data != null && !data.isEmpty()) {
            if (data.contains("http")) {
                webView.loadUrl(data);
            } else {
                webView.loadData(data);
            }
        } else {
            webView.loadUrl(url);
        }
    }

    /**
     * 调用js方法
     * @param jsFunName 调用js的方法名
     * @param callback
     */
    public void evaluateJavascript(String jsFunName, ValueCallback callback){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            if(callback==null){
//                callback=new ValueCallback<String>() {
//                    @Override
//                    public void onReceiveValue(String value) {
//                        //此处为 js 返回的结果
//                    }
//                };
//            }
            webView.getWebView().evaluateJavascript("javascript:"+jsFunName+"()", callback);
        }else{
            webView.getWebView().loadUrl("javascript:"+jsFunName+"()");
        }
    }

    /**
     * 通过addJavascriptInterface()将Java对象映射到JS对象
     */
    public void addJavascriptInterface(){
        // 通过addJavascriptInterface()将Java对象映射到JS对象
        //参数1：Javascript对象名
        //参数2：Java对象名
        webView.getWebView().addJavascriptInterface(new AndroidtoJs(), "test");//AndroidtoJS类对象映射到js的test对象

    }

    // 继承自Object类
    public class AndroidtoJs extends Object {

        // 定义JS需要调用的方法
        // 被JS调用的方法必须加入@JavascriptInterface注解
        @JavascriptInterface
        public void hello(String msg) {
            System.out.println("JS调用了Android的hello方法");
        }


        @JavascriptInterface
        public void showAdv() {
            System.out.println("JS调用了Android的showAdv方法");
        }
    }
}
