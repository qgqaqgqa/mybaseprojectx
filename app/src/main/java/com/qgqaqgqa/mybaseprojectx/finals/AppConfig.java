package com.qgqaqgqa.mybaseprojectx.finals;

import android.content.Context;
import android.os.Environment;

import com.blankj.utilcode.util.SPUtils;
import com.qgqaqgqa.mybaseprojectx.MyApplication;
import com.qgqaqgqa.mybaseprojectx.R;

import java.io.File;

/**
 * ClassName:AppConfig Function:系统配置文件目录，配置系统的默认固定参数.
 * Description: 系统配置文件目录，配置系统的默认固定参数.
 * User: Created by 钱昱凯
 * Date: 2018/2/8 0008
 * Time: 14:48
 * Email:342744291@qq.com
 */
public class AppConfig {
    public static final int REQUEST_ADD = 0;//添加
    public static final int REQUEST_EDIT = REQUEST_ADD+1;//编辑
    public static final int REQUEST_SELECT = REQUEST_EDIT+1;//选择
    public static final int REQUEST_OFFICE = REQUEST_SELECT+1;//选择单位
    public static final int REQUEST_RESUME = REQUEST_OFFICE + 1;//重新启用
    public static final int REQUEST_SELECT_FILE = REQUEST_RESUME+1;//选择文件
    public static final int REQUEST_PROJECT_FROM = REQUEST_SELECT_FILE+1;//新建项目
    public static final int REQUEST_PROJECT_INFO = REQUEST_PROJECT_FROM + 1;//编辑项目
    public static final int REQUEST_USERS = REQUEST_PROJECT_INFO+1;//选择人员
    public static final int REQUEST_FACE = REQUEST_USERS+1;//打开人脸比对
    public static final int REQUEST_SUPERVISION_LOG_EDIT = REQUEST_FACE + 1;//监理日志新增编辑
    public static final int REQUEST_SAFETY_LOG_EDIT = REQUEST_SUPERVISION_LOG_EDIT + 1;//安监日志新增编辑
    public static final int REQUEST_AUTH = REQUEST_SAFETY_LOG_EDIT+1;//授权
    public static final int REQUEST_HELP = REQUEST_AUTH + 1;//远程帮助
    public static final int REQUEST_MESSAGE = REQUEST_HELP+1;//消息管理
    public static final int REQUEST_SETTING = REQUEST_MESSAGE + 1;//打开设置
    public static final int REQUEST_FIRST_FACE = REQUEST_SETTING + 1;//第一次人脸录入
    public static final int REQUEST_FIND_DEVICE = REQUEST_FIRST_FACE+1;//查找设备
    public static final int REQUEST_PROJECT_NAME = REQUEST_FIND_DEVICE+1;//查找项目名称
    public static final int REQUEST_BIND_PHONE = REQUEST_PROJECT_NAME+1;//绑定手机号
    public static final int REQUEST_BIND_USER = REQUEST_BIND_PHONE + 1;//绑定用户
    public static final int REQUEST_BIND_REGISTER = REQUEST_BIND_USER + 1;//绑定注册
    public static final int REQUEST_OPEN = REQUEST_BIND_REGISTER + 1;//打开会议
    public static final int REQUEST_CREATE = REQUEST_OPEN + 1;//创建会议
    public static final int REQUEST_EDIT_PASSWORD = REQUEST_CREATE;//重置密码
    public static final int REQUEST_DEVICE = REQUEST_EDIT_PASSWORD + 1;//选择设备
    public static final int REQUEST_LOGIN = 999;

    /**
     * 选择相册
     */
    public static final int REQUEST_CODE_PHOTO_ALBUM = REQUEST_DEVICE + 1;
    /**
     * 选择多图
     */
    public static final int REQUEST_CODE_MULTI_ALBUM = REQUEST_CODE_PHOTO_ALBUM + 1;
    /**
     * 添加课表
     */
    public static final int REQUEST_ADD_SCHEDULE = REQUEST_CODE_MULTI_ALBUM + 1;

    //系统缓存字段
    public static final String SP_IS_SAVE_USERNAME = "sp_is_save_username";
    public static final String SP_IS_SAVE_PASSWORD = "sp_is_save_password";
    public static final String SP_USERNAME = "sp_username";
    public static final String SP_USERPWD = "sp_userpwd";
    public static final String SP_OLD_USERNAME = "sp_old_username";
    public static final String SP_OLD_USERPWD = "sp_old_userpwd";
    public static final String SP_SESSIONID = "sp_sessionid";
    public static final String SP_IS_MEMBER = "sp_is_member";//是否是会员
    public static final String SP_LAST_TRIAL_TIME = "sp_last_trial_time";
    public static final String SP_LAST_TRIAL_NUM = "sp_last_trial_num";
    public static final String SP_USER_HEAD = "sp_user_head";
    public static final String SP_BG_ALPHA = "sp_bg_alpha";
    public static final String SP_GRID_ALPHA = "sp_grid_alpha";
    public static final String SP_GRID_SHOW = "sp_grid_show";

    public static String SWITCH_GUIDE = "switch_guide";//监控模块功能开关
    public static final String KEY_HAVE_UPDATE_APK = "key_have_update_apk";//有更新包



    //服务器基本信息
    public static final String SECRETKEY = "dlqx,wskj,online";

    //服务器基本信息
    //当前连接服务器模式，测试模式还是产线模式,正式版的时候需要改成false.
    public static final boolean TEST_MODE = true;

    ///////////////////////////////////////////////////////////////////////////
    // 测试的API头地址.
    ///////////////////////////////////////////////////////////////////////////
    public static final String TEST_ZAPI_URL = "https://test99.rhinox.cn";

    ///////////////////////////////////////////////////////////////////////////
    // 产线的API头地址.
    ///////////////////////////////////////////////////////////////////////////
    public static final String ONLINE_ZAPI_URL = "https://test99.rhinox.cn";

    ///////////////////////////////////////////////////////////////////////////
    // App使用的API头地址.
    ///////////////////////////////////////////////////////////////////////////
    public static final String HOST_URL = (TEST_MODE?TEST_ZAPI_URL:ONLINE_ZAPI_URL);
    public static final String ZAPI_URL = HOST_URL +"/standard/";

    //应用缓存文件基本信息，程序在手机SDK中的主缓存目录.
    public static final String APP_PATH = Environment.getExternalStorageDirectory().getPath()+
            File.separator+ MyApplication.getInstance().getApplicationContext().getResources()
            .getString(R.string.app_name);

    //程序在手机SDK中的缓存目录
    public static final String DIR_CACHE = APP_PATH + File.separator + "cache";

    //程序在手机SDK中的音频缓存目录.
    public static final String DIR_SOUND = APP_PATH + File.separator + "sound";

    //程序在手机SDK中的图片缓存目录.
    public static final String DIR_IMG = APP_PATH + File.separator + "image";

    //程序在手机SDK中的视频缓存目录.
    public static final String DIR_VIDEO = APP_PATH + File.separator + "video";

    //程序在手机SDK中的设备照片缓存目录.
    public static final String DIR_DEVICE = APP_PATH + File.separator + "device";
    //程序在手机SDK中的图片缓存目录.
    public static final String DIR_LOG_IMG = APP_PATH + File.separator + "logimage";

    //程序在手机SDK中的压缩图片缓存目录
    public static final String DIR_CPIMG = DIR_IMG + File.separator + "cpimg";

    //程序在手机SDK中的文件缓存目录
    public static final String DIR_FILE = APP_PATH + File.separator + "file";


    public static final String DIR_BAIDUMAP = APP_PATH + File.separator + "baidumap";

    public static final String DIR_FACE = APP_PATH + File.separator + "face";
    public static final String DIR_FACE_IMG = DIR_FACE + File.separator + "imgs";
    public static final String DIR_FACE_FEATURE = DIR_FACE + File.separator + "features";
    public static final String DIR_IDCARD = APP_PATH + File.separator + "idcard";


    public static boolean getSwitch(String switchMenu) {
        return SPUtils.getInstance().getBoolean(switchMenu, false);
    }


}