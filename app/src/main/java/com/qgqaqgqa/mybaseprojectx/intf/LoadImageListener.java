package com.qgqaqgqa.mybaseprojectx.intf;

import android.graphics.drawable.Drawable;

/**
 * 加载图片监听
 * Created by WangXY on 2015/11/17.16:35.
 */
public interface LoadImageListener {

    /**
     * 开始加载监听
     */
    public void onLoadingStart();

    /**
     * 加载失败监听
     *
     * @param e
     * @param errorDrawable
     */
    public void onLoadingFailed(Exception e, Drawable errorDrawable);

//    /**
//     * 加载成功监听
//     *
//     * @param resource
//     * @param animation
//     */
//    public void onLoadingComplete(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation);
}
