package com.qgqaqgqa.mybaseprojectx.view;
import android.app.Activity;
import android.view.View;
import butterknife.ButterKnife;

/**
 * 底部栏
 * User: Created by 钱昱凯
 * Date: 2018/3/8 0008
 * Time: 11:29
 * Email:342744291@qq.com
 */
public class CommonBottomBar implements View.OnClickListener {
    public static final int MENU_POSITION1 = 0;
    public static final int MENU_POSITION2 = MENU_POSITION1 + 1;
    public static final int MENU_POSITION3 = MENU_POSITION2 + 1;
    public static final int MENU_POSITION4 = MENU_POSITION3 + 1;
    public static final int MENU_POSITION5 = MENU_POSITION4 + 1;
    private int colorSel = -1;
    private int colorNor = -1;
    private Activity mActivity;

//    @BindView(R.id.footer_menu_home)
//    LinearLayout mLlMenu1;
//    @BindView(R.id.footer_menu_home_btn_icon)
//    ImageView mIvMenu1;
//    @BindView(R.id.footer_menu_home_btn_icon_text)
//    TextView mTvMenu1;
//    @BindView(R.id.footer_menu_mall)
//    LinearLayout mLlMenu2;
//    @BindView(R.id.footer_menu_mall_btn_icon)
//    ImageView mIvMenu2;
//    @BindView(R.id.footer_menu_mall_btn_icon_text)
//    TextView mTvMenu2;
//    @BindView(R.id.footer_menu_so)
//    LinearLayout mLlMenu3;
//    @BindView(R.id.footer_menu_so_btn_icon)
//    ImageView mIvMenu3;
//    @BindView(R.id.footer_menu_so_btn_icon_text)
//    TextView mTvMenu3;
//    @BindView(R.id.footer_menu_cms)
//    LinearLayout mLlMenu4;
//    @BindView(R.id.footer_menu_cms_btn_icon)
//    ImageView mIvMenu4;
//    @BindView(R.id.footer_menu_cms_btn_icon_text)
//    TextView mTvMenu4;
//    @BindView(R.id.footer_menu_ucenter)
//    LinearLayout mLlMenu5;
//    @BindView(R.id.footer_menu_ucenter_btn_icon)
//    ImageView mIvMenu5;
//    @BindView(R.id.footer_menu_ucenter_btn_icon_text)
//    TextView mTvMenu5;

    private int currentPosition = 0;

    private OnClickMenuListener listener;
    private String menuName="";

    public CommonBottomBar(Activity activity) {
        this(activity, null);
    }

    public CommonBottomBar(Activity activity, OnClickMenuListener listener) {
        this.mActivity = activity;
        this.listener = listener;
        ButterKnife.bind(this, activity);
//        mLlMenu1.setOnClickListener(this);
//        mLlMenu2.setOnClickListener(this);
//        mLlMenu3.setOnClickListener(this);
//        mLlMenu4.setOnClickListener(this);
//        mLlMenu5.setOnClickListener(this);
//        mIvMenu1.setImageResource(R.drawable.sl_footer_menus1_icon);
//        mIvMenu2.setImageResource(R.drawable.sl_footer_menus2_icon);
//        mIvMenu3.setImageResource(R.drawable.sl_footer_menus3_icon);
//        mIvMenu4.setImageResource(R.drawable.sl_footer_menus4_icon);
//        mIvMenu5.setImageResource(R.drawable.sl_footer_menus5_icon);
//        colorSel = mActivity.getResources().getColor(R.color.colorPrimary);
//        colorNor = mActivity.getResources().getColor(R.color.colorGray1);
    }

    /**
     * @param positionOrId position或viewid
     */
    public void clickMenu(int positionOrId) {
//        switch (positionOrId) {
//            default:
//            case MENU_POSITION1:
//            case R.id.footer_menu_home:
//            case R.id.footer_menu_home_btn_icon:
//            case R.id.footer_menu_home_btn_icon_text:
//                currentPosition = MENU_POSITION1;
//                menuName=mTvMenu1.getText().toString();
//                break;
//            case MENU_POSITION2:
//            case R.id.footer_menu_mall:
//            case R.id.footer_menu_mall_btn_icon:
//            case R.id.footer_menu_mall_btn_icon_text:
//                currentPosition = MENU_POSITION2;
//                menuName=mTvMenu2.getText().toString();
//                break;
//            case MENU_POSITION3:
//            case R.id.footer_menu_so:
//            case R.id.footer_menu_so_btn_icon:
//            case R.id.footer_menu_so_btn_icon_text:
//                currentPosition = MENU_POSITION3;
//                menuName=mTvMenu3.getText().toString();
//                break;
//            case MENU_POSITION4:
//            case R.id.footer_menu_cms:
//            case R.id.footer_menu_cms_btn_icon:
//            case R.id.footer_menu_cms_btn_icon_text:
//                currentPosition = MENU_POSITION4;
//                menuName=mTvMenu4.getText().toString();
//                break;
//            case MENU_POSITION5:
//            case R.id.footer_menu_ucenter:
//            case R.id.footer_menu_ucenter_btn_icon:
//            case R.id.footer_menu_ucenter_btn_icon_text:
//                currentPosition = MENU_POSITION5;
//                menuName=mTvMenu5.getText().toString();
//                break;
//        }
//        if (listener != null) {
//            listener.onClickMenu(currentPosition);
//        }
//        showMenu(currentPosition);
//
//
//        HashMap<String, String> map = new HashMap<>();
//        map.put("name", menuName);
//        MobclickAgent.onEvent(mActivity, UmengEventId.CLICK_MAIN_BOTTOM_MENU, map);

    }

    /**
     * @param positionOrId position或viewid
     */
    public void showMenu(int positionOrId) {
//        currentPosition = positionOrId;
//        mIvMenu1.setSelected(false);
//        mIvMenu2.setSelected(false);
//        mIvMenu3.setSelected(false);
//        mIvMenu4.setSelected(false);
//        mIvMenu5.setSelected(false);
//        mTvMenu1.setTextColor(colorNor);
//        mTvMenu2.setTextColor(colorNor);
//        mTvMenu3.setTextColor(colorNor);
//        mTvMenu4.setTextColor(colorNor);
//        mTvMenu5.setTextColor(colorNor);
//
//        switch (positionOrId) {
//            default:
//            case MENU_POSITION1:
//            case R.id.footer_menu_home:
//            case R.id.footer_menu_home_btn_icon:
//            case R.id.footer_menu_home_btn_icon_text:
//                mIvMenu1.setSelected(true);
//                mTvMenu1.setTextColor(colorSel);
//                break;
//            case MENU_POSITION2:
//            case R.id.footer_menu_mall:
//            case R.id.footer_menu_mall_btn_icon:
//            case R.id.footer_menu_mall_btn_icon_text:
//                mIvMenu2.setSelected(true);
//                mTvMenu2.setTextColor(colorSel);
//                break;
//            case MENU_POSITION3:
//            case R.id.footer_menu_so:
//            case R.id.footer_menu_so_btn_icon:
//            case R.id.footer_menu_so_btn_icon_text:
//                mIvMenu3.setSelected(true);
//                mTvMenu3.setTextColor(colorSel);
//                break;
//            case MENU_POSITION4:
//            case R.id.footer_menu_cms:
//            case R.id.footer_menu_cms_btn_icon:
//            case R.id.footer_menu_cms_btn_icon_text:
//                mIvMenu4.setSelected(true);
//                mTvMenu4.setTextColor(colorSel);
//                break;
//            case MENU_POSITION5:
//            case R.id.footer_menu_ucenter:
//            case R.id.footer_menu_ucenter_btn_icon:
//            case R.id.footer_menu_ucenter_btn_icon_text:
//                mIvMenu5.setSelected(true);
//                mTvMenu5.setTextColor(colorSel);
//                break;
//        }
    }

    @Override
    public void onClick(View view) {
        clickMenu(view.getId());
    }

    public interface OnClickMenuListener {
        void onClickMenu(int position);
    }
}