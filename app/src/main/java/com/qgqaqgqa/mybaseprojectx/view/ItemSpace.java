package com.qgqaqgqa.mybaseprojectx.view;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SizeUtils;

public class ItemSpace extends RecyclerView.ItemDecoration {

        private int mTop = 0;
        private int mLeft = 0;
        private int mRight = 0;
        private int mBottom = 0;

        public ItemSpace( int left, int top, int right, int bottom) {
            this.mLeft = SizeUtils.dp2px(left);
            this.mTop = SizeUtils.dp2px(top);
            this.mRight = SizeUtils.dp2px(right);
            this.mBottom = SizeUtils.dp2px(bottom);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.top = mTop;
            outRect.left = mLeft;
            outRect.right = mRight;
            outRect.bottom = mBottom;
        }
    }