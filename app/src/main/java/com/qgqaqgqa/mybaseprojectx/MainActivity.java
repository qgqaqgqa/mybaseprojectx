package com.qgqaqgqa.mybaseprojectx;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.qgqaqgqa.mybaseprojectx.base.BaseProtocolActivity;
import com.qgqaqgqa.mybaseprojectx.finals.UrlConstants;
import com.qgqaqgqa.mybaseprojectx.net.BaseNetModel;
import com.qgqaqgqa.mybaseprojectx.net.ApiManager;
import com.qgqaqgqa.mybaseprojectx.net.IBaseHttpCallBack;

import org.reactivestreams.Subscription;

public class MainActivity extends BaseProtocolActivity implements IBaseHttpCallBack {

    public MainActivity() {
        super(R.layout.activity_main);
    }

    @Override
    public void initViews() {
        addDisposable(ApiManager.getInstance().get(Object.class, UrlConstants.common.CommonSystemSettings.COMMONSYSTEMSETTINGS_GETKEYVALUELISTS,this));
    }

    @Override
    public void onSucess(String url, BaseNetModel response) {

    }

    @Override
    public void onFinish() {

    }
}
